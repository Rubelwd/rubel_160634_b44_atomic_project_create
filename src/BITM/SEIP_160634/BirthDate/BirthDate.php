<?php

namespace App\BirthDate;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class BirthDate extends DB
{
    private $id;
    private $user_name;
    private $birth_date;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
            $this->id = $postData['id'];
        }

        if(array_key_exists('user_name',$postData)) {
            $this->user_name = $postData['user_name'];
        }

        if(array_key_exists('birth_date',$postData)) {
            $this->birth_date = $postData['birth_date'];
        }
    }

    public function store() {
        $arrData = array($this->user_name,$this->birth_date);
        $sql = "INSERT INTO birth_date (user_name,birth_date) VALUES(?,?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully!!!");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully!!!");
        Utility::redirect("create.php");
    }

    public function index() {
        $sql = "SELECT * FROM birth_date WHERE soft_delete = 'No'";
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetchAll();
    }

    public function view() {
        $sql = "SELECT * FROM birth_date WHERE id = ".$this->id;
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetch();
    }

    public function trashed() {
        $sql = "SELECT * FROM birth_date WHERE soft_delete = 'Yes'";
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetchAll();
    }



}