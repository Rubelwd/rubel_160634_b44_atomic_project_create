<?php


namespace App\BookTitle;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class BookTitle extends DB
{
    private $id;
    private $book_name;
    private $author_name;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
        $this->id = $postData['id'];
        }

        if(array_key_exists('book_name',$postData)) {
            $this->book_name = $postData['book_name'];
        }

        if(array_key_exists('author_name',$postData)) {
            $this->author_name = $postData['author_name'];
        }
    }

    public function store() {
        $arrData = array($this->book_name,$this->author_name);
        $sql = "INSERT INTO book_title (book_name,author_name) VALUES(?,?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully!!!");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully!!!");
        Utility::redirect("index.php");
    }

    public function index() {
        $sql = "SELECT * FROM book_title WHERE soft_delete = 'No'";
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetchAll();
    }

    public function view() {
        $sql = "SELECT * FROM book_title WHERE id = ".$this->id;
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetch();
    }

    public function trashed() {
        $sql = "SELECT * FROM book_title WHERE soft_delete = 'Yes'";
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetchAll();
    }

    public function update() {
        $arrData = array($this->book_name,$this->author_name);
        $sql = "UPDATE book_title SET book_name = ?, author_name = ? WHERE id = ".$this->id;
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Updated Successfully!!!");
        else
            Message::message("Failed! Data Has Not Been Updated Successfully!!!");
        Utility::redirect("index.php");
    }


    public function trash() {
        $sql = "UPDATE book_title SET soft_delete = 'Yes' WHERE id = ".$this->id;
        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully!!!");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted Successfully!!!");
        Utility::redirect("trashed.php");
    }

    public function delete() {
        $sql = "DELETE FROM book_title WHERE id = ".$this->id;
        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Permanently Deleted Successfully!!!");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted Successfully!!!");
        Utility::redirect("index.php");
    }

    public function recover() {
        $sql = "UPDATE book_title SET soft_delete = 'No' WHERE id = ".$this->id;
        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Recovered Successfully!!!");
        else
            Message::message("Failed! Data Has Not Been Recovered Successfully!!!");
        Utility::redirect("index.php");
    }




}