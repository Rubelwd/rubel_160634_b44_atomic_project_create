<?php


namespace App\Email;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class Email extends DB
{
    private $id;
    private $email_address;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
        $this->id = $postData['id'];
        }

        if(array_key_exists('email_address',$postData)) {
            $this->email_address = $postData['email_address'];
        }
    }

    public function store() {
        $arrData = array($this->email_address);
        $sql = "INSERT INTO email (email_address) VALUES (?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully!!!");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully!!!");
        Utility::redirect("create.php");
    }

    public function index() {
        $sql = "SELECT * FROM email WHERE soft_delete = 'No'";
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetchAll();
    }

    public function view() {
        $sql = "SELECT * FROM email WHERE id = ".$this->id;
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetch();
    }

    public function trashed() {
        $sql = "SELECT * FROM email WHERE soft_delete = 'Yes'";
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetchAll();
    }


}