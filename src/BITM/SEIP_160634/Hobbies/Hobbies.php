<?php


namespace App\Hobbies;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class Hobbies extends DB
{
    private $id;
    private $student_name;
    private $hobbies;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
            $this->id = $postData['id'];
        }

        if(array_key_exists('student_name',$postData)) {
            $this->student_name = $postData['student_name'];
        }

        if(array_key_exists('hobby',$postData)) {
            $this->hobbies = $postData['hobby'];
        }
    }




    public function store() {
        $arrData = array($this->student_name,$this->hobbies);
        $sql = "INSERT INTO hobbies (student_name,hobbies) VALUES (?,?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully!!!");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully!!!");
        Utility::redirect("create.php");
    }

    public function index() {
        $sql = "SELECT * FROM hobbies WHERE soft_delete = 'No'";
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetchAll();
    }

    public function view() {
        $sql = "SELECT * FROM hobbies WHERE id = ".$this->id;
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetch();
    }

    public function trashed() {
        $sql = "SELECT * FROM hobbies WHERE soft_delete = 'Yes'";
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetchAll();
    }


}