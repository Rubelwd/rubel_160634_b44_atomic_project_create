<?php


namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class ProfilePicture extends DB
{
    private $id;
    private $name;
    private $fileName;
    private $tmpName;



    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
        $this->id = $postData['id'];
        }


        if(array_key_exists('name',$postData)) {
            $this->name = $postData['name'];

        }

        if(array_key_exists('file_upload',$postData)){
            if(array_key_exists("name",$postData)){
                $this->fileName = $postData['file_upload']['name'];
            }
        }
        if(array_key_exists('file_upload',$postData)){
            if(array_key_exists("name",$postData)){
                $this->tmpName = $postData['file_upload']['tmp_name'];
            }
        }
    }

    public function store() {

        $arrData = array($this->name,$this->fileName);
        $sql = "INSERT INTO profile_picture (name,picture) VALUES (?,?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully!!!");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully!!!");
        Utility::redirect("create.php");

    }

    public function index() {
        $sql = "SELECT * FROM profile_picture WHERE soft_delete = 'No'";
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetchAll();
    }

    public function view() {
        $sql = "SELECT * FROM profile_picture WHERE id = ".$this->id;
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetch();
    }

    public function trashed() {
        $sql = "SELECT * FROM profile_picture WHERE soft_delete = 'Yes'";
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetchAll();
    }

    public function moveFile() {
        move_uploaded_file($this->tmpName, "../../../../includes/uploads/".$this->fileName);
    }


}