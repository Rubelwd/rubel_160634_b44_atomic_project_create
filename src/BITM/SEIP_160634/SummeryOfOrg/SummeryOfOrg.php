<?php


namespace App\SummeryOfOrg;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class SummeryOfOrg extends DB
{
    private $id;
    private $org_name;
    private $summery;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
        $this->id = $postData['id'];
        }

        if(array_key_exists('org_name',$postData)) {
            $this->org_name = $postData['org_name'];
        }

        if(array_key_exists('summery',$postData)) {
            $this->summery = $postData['summery'];
        }
    }

    public function store() {
        $arrData = array($this->org_name,$this->summery);
        $sql = "INSERT INTO summery_of_org (org_name,summery) VALUES (?,?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully!!!");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully!!!");
        Utility::redirect("create.php");
    }

    public function index() {
        $sql = "SELECT * FROM summery_of_org WHERE soft_delete = 'No'";
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetchAll();
    }

    public function view() {
        $sql = "SELECT * FROM summery_of_org WHERE id = ".$this->id;
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetch();
    }

    public function trashed() {
        $sql = "SELECT * FROM summery_of_org WHERE soft_delete = 'Yes'";
        $statement = $this->DBH->query($sql);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetchAll();
    }


}