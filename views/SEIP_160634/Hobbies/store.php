<?php
require_once("../../../vendor/autoload.php");
use App\Hobbies\Hobbies;

$objHobbies = new Hobbies();

$str = implode(",",$_POST['hobby']);
$_POST['hobby'] = $str;

$objHobbies->setData($_POST);
$objHobbies->store();