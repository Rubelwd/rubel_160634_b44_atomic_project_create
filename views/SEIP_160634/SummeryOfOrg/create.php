<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/main.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="container-fluid">
    <nav class="navbar navbar-inverse">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../index.php">Administrator</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <form class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                    <a class="btn btn-primary" href="index.php">Logout</a>
                </form>
            </div><!-- /.navbar-collapse -->

        </div><!-- /.container -->
    </nav><!-- End Navbar -->
</div>

<div class="main-content">
    <div class="row">
        <div class="col-md-2">
            <div class="sidebar"><!-- Start Sidebar -->
                <ul class="nav nav-pills nav-stacked">
                    <li role="presentation" class="active"><a href="../Birthdate/create.php">Add Birth Day</a></li>
                    <li role="presentation"><a href="../Booktitle/create.php">Add Book Title</a></li>
                    <li role="presentation"><a href="../City/create.php">Add City Name</a></li>
                    <li role="presentation"><a href="../Email/create.php">Add Email Address</a></li>
                    <li role="presentation"><a href="../Gender/create.php">Add Gender</a></li>
                    <li role="presentation"><a href="../Hobbies/create.php">Add Hobbies</a></li>
                    <li role="presentation"><a href="../ProfilePicture/create.php">Add Profile Picture</a></li>
                    <li role="presentation"><a href="create.php">Add Summery</a>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="">Updsate Summery</a></li>
                            <li><a href="index.php">View Summery</a></li>
                            <li><a href="trashed.php">View Trash Summery</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- End Sidebar -->
        </div>
        <div class="col-md-9">
            <div class="content">
                <?php
                require_once("../../../vendor/autoload.php");
                if(!isset($_SESSION)) session_start();
                use App\Message\Message;

                $msg = Message::message();

                echo "<div id='message' style='color: green;font-weight: bold'>  $msg </div>";
                ?>
                <form action="store.php" method="post">
                    <div class="form-group">
                        <label for="email_address">Name Of Organization</label>
                        <input type="text" class="form-control" name="org_name">
                    </div>
                    <label for="summery_of_org">Organization Summery</label>
                    <textarea class="form-control" rows="3" name="summery"></textarea><br>
                    <input type="submit" value="Submit" name="submit_form" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
</div>



<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>
    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>
</body>
</html>
